from django.test import TestCase, Client
from django.urls import resolve
from django.apps import apps
from .apps import Story8Config
from .views import find_book, book_list
# Create your tests here.
class TestStory8(TestCase):
	def test_if_find_book_url_exist(self):
		response = Client().get('/find_book/')
		self.assertEquals(response.status_code, 200)

	def test_if_data_url_exist(self):
		response = Client().get('/data/?q=')
		self.assertEquals(response.status_code, 200)

	def test_if_find_book_url_uses_story8_template(self):
		response = Client().get('/find_book/')
		self.assertTemplateUsed(response, 'story8.html')

	def test_find_book_url_resolves_to_find_book_view(self):
		found = resolve('/find_book/')
		self.assertEquals(found.func, find_book)

	def test_data_url_resolves_to_book_list_view(self):
		found = resolve('/data/')
		self.assertEquals(found.func, book_list)

	def test_app_name(self):
		self.assertEquals(Story8Config.name, 'story8')
		self.assertEquals(apps.get_app_config('story8').name, 'story8')
