from django.urls import include, path
from .views import myblog


urlpatterns = [
    path('myblog/', myblog, name='myblog')
]

