from django.test import TestCase, Client
from django.urls import resolve


from .views import myblog
# Create your tests here.
class TestStory7(TestCase):
	def test_if_about_me_url_exist(self):
		response = Client().get('/myblog/')
		self.assertEquals(response.status_code, 200)

	def test_if_about_me_url_uses_story7_template(self):
		response = Client().get('/myblog/')
		self.assertTemplateUsed(response, 'story7.html')

	def test_about_me_url_resolves_to_about_me_view(self):
		found = resolve('/myblog/')
		self.assertEquals(found.func, myblog)


