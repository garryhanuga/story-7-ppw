from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm
from .forms import CreateUserForm


# Create your views here.
def content(request):
	return render(request, 'story9.html')

def signup(request):
	args= {}
	if request.method == 'POST':
		form=UserCreationForm(request.POST)
		if form.is_valid():
			form.save()
			warn= "Yeay, you now have an account!"
			args['warn']=warn

			return render(request,'signup.html',{'warn':warn,'form':form})
	else:
		form=UserCreationForm()

	return render(request, 'signup.html', { 'form' : form })


