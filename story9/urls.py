from django.contrib import admin
from django.urls import path, include
from . import views

urlpatterns = [
    path('content/', views.content, name='content'),
    path('signup/',views.signup, name='signup'),
]
