$(document).ready(function() {
	$("#keyword").on("keyup", function(e) {
		var to_find = e.currentTarget.value;
		console.log(to_find);

		$.ajax({
			url: '/data?q=' + to_find,
			success: function(data) {
				var content = data.items;
				console.log(content);
				$(".table").empty();
				for (i=0; i<content.length; i++) {
					if(i==0) {
						$(".table").append(
							"<thead><tr><th scope='col'>No</th><th scope='col'>Cover</th>\
							<th scope='col'>Title</th><th scope='col'>Author</th>\
							<th scope='col'>Category</th><th scope='col'>Book Description</th></tr></thead>");
					}
					var title = content[i].volumeInfo.title;
					var author = content[i].volumeInfo.authors;
					var pic = content[i].volumeInfo.imageLinks.smallThumbnail;
					var category = content[i].volumeInfo.categories;
					var original_desc = content[i].volumeInfo.description;
					var url = content[i].volumeInfo.infoLink;

					if (original_desc) {
						var desc = content[i].volumeInfo.description.substring(0, 250);
						if (original_desc.length > 250) {
							desc+="...";
						}
					}
					$(".table").append(
						"<tr class='tr'> <th scope='row'>" + (i+1) + "</th> <td><a target='_blank' href="
						+ url + "><img src=" + pic + "></td></a><td class='title'><a target='_blank' href="
						+ url + ">" + title + "</td></a><td class='author'>" + author +"</td> <td class='category'>"
						+ category + "</td><td>" + desc + "</td></tr>");
				}
			}
		});
	})
})
