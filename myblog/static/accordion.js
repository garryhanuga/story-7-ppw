$(document).ready(function() {

    $('.accordion').on('click', event => {
        if ($(event.target).is('img')) {
            event.preventDefault();
            return;
        }

        const $accordion = $(event.currentTarget);
        $($accordion).toggleClass("active");
    });


    $('.down').on('click', event => {
        const $accordion = $(event.currentTarget).parents('.accordion');
        if(($accordion).next().is('.accordion')) {
        $($accordion).before($($accordion).next());            
        }
    });

    $('.up').on('click', event => {
        const $accordion = $(event.currentTarget).parents('.accordion');
        $($accordion).after($($accordion).prev());
    });

});
