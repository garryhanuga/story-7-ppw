from django.test import TestCase, Client
from django.urls import resolve
from .apps import Story9Config


from .views import content, signup
# Create your tests here.
class TestStory7(TestCase):
	def test_if_content_url_exist(self):
		response = Client().get('/content/')
		self.assertEquals(response.status_code, 200)

	def test_if_content_template(self):
		response = Client().get('/content/')
		self.assertTemplateUsed(response, 'story9.html')

	def test_content_view(self):
		found = resolve('/content/')
		self.assertEquals(found.func, content)

	def test_if_signup_url_exist(self):
		response = Client().get('/signup/')
		self.assertEquals(response.status_code, 200)

	def test_if_signup_template(self):
		response = Client().get('/signup/')
		self.assertTemplateUsed(response, 'signup.html')

	def test_signup_view(self):
		found = resolve('/signup/')
		self.assertEquals(found.func, signup)
